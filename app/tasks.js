const express = require('express');
const auth = require('../middleware/auth');
const Tasks = require('../models/Task');
const router = express.Router();

router.post('/', auth, (req, res) => {
    const data = req.body;
    data.user = req.user._id;
    const task = new Tasks(data);
    task.save()
        .then(result => res.send(result))
        .catch(error => res.sendStatus(error))
});

router.get('/', auth, (req, res) => {
    Tasks.find({user: req.user._id})
        .then(result => res.send(result))
        .catch(error => res.send(error))
});

router.put('/:id', auth, async (req, res) => {
    const reqData = req.body;
    delete reqData.user;

    const task = await Tasks.findById(req.params.id);
    task.title = reqData.title ? reqData.title : task.title;
    task.description = reqData.description ? reqData.description : task.description;
    task.status = reqData.status ? reqData.status : task.status;
    await task.save();
    res.send(task)

});

router.delete('/:id', auth, (req, res) => {
    Tasks.findById(req.params.id).then(
        result => Tasks.deleteOne(result)
    ).catch(error => res.send(error))
    res.send({message: "Success deleted"})
});

module.exports = router;