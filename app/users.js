const express = require('express');
const Users = require('../models/User');

const router = express.Router();

router.post('/', (req, res) => {
    const user = new Users(req.body);
    user.generateToken();
    user.save()
        .then(res.send({token: user.token}))
        .catch(error => res.status(400).send(error))
});


router.post('/sessions', async (req, res) => {
    const user = await Users.findOne({username: req.body.username});

    if (!user) {
        return res.status(400).send({error: "Username not found"});
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
        return res.status(400).send({error: "Password is wrong!"});
    }

    user.generateToken();

    await user.save();

    return res.send({token: user.token});
});

module.exports = router;