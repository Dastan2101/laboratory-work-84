const express = require('express');
const mongoose = require('mongoose');
const config = require('./config');

const users = require('./app/users');
const tasks = require('./app/tasks');

const app = express();

app.use(express.json());

const port = 8000;

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
    app.use('/users', users);
    app.use('/tasks', tasks);

    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });

});

