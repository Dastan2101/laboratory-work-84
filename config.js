module.exports = {
    dbUrl: 'mongodb://localhost/toDo_list',
    mongoOptions: {useNewUrlParser: true, useCreateIndex: true}
};
